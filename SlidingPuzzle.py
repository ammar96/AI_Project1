import copy
import random
import numpy as np
from enum import Enum
from Node import Node
from Problem import Problem


class SlidingPuzzle(Problem):
    solvable = None
    n = None
    goalState = None
    initialState = None
    allActions = Enum('allActions', 'MOVE_UP MOVE_DOWN MOVE_RIGHT MOVE_LEFT')

    def __init__(self, n=3):
        self.n = n
        self.generateRandomInitialState()
        self.readInitialStateFromInput()
        # self.generateInitialNode()
        self.goalState = [[(n * y + x) for x in range(self.n)] for y in range(self.n)]
        self.solvable = self.isSolvable()
        if not self.solvable:  # swap last two tiles in goal state
            print("given puzzle is not solvable.\ntrying to find a path to alternative goal ...")
            index1, index2 = self.getIndex(Node(self.goalState), n * n - 1), self.getIndex(Node(self.goalState),
                                                                                           n * n - 2)
            self.goalState[index1[0]][index1[1]], self.goalState[index2[0]][index2[1]] = self.goalState[index2[0]][
                                                                                             index2[1]], \
                                                                                         self.goalState[index1[0]][
                                                                                             index1[1]]

    def getResult(self, action, node):
        newNode = Node(copy.deepcopy(node.state), node.g + self.actionCost(action), node)
        i, j = SlidingPuzzle.getIndex(newNode, 0)[0], SlidingPuzzle.getIndex(newNode, 0)[1]
        if action == self.allActions.MOVE_UP:
            newNode.state[i][j] = newNode.state[i - 1][j]
            newNode.state[i - 1][j] = 0
        elif action == self.allActions.MOVE_DOWN:
            newNode.state[i][j] = newNode.state[i + 1][j]
            newNode.state[i + 1][j] = 0
        elif action == self.allActions.MOVE_RIGHT:
            newNode.state[i][j] = newNode.state[i][j + 1]
            newNode.state[i][j + 1] = 0
        else:
            newNode.state[i][j] = newNode.state[i][j - 1]
            newNode.state[i][j - 1] = 0
        return newNode

    def getReversedActionResult(self, action, node):
        return self.getResult(action, node)

    @staticmethod
    def pathCost(node):
        return node.g

    def getActions(self, node):
        actions = []
        i = SlidingPuzzle.getIndex(node, 0)[0]
        j = SlidingPuzzle.getIndex(node, 0)[1]
        if i != 0:
            actions.append(self.allActions.MOVE_UP)
        if i != self.n - 1:
            actions.append(self.allActions.MOVE_DOWN)
        if j != self.n - 1:
            actions.append(self.allActions.MOVE_RIGHT)
        if j != 0:
            actions.append(self.allActions.MOVE_LEFT)
        return actions

    def getReversedActions(self, node):
        return self.getActions(node)

    def actionCost(self, action):
        return 1

    def goalTest(self, node):
        return self.goalState == node.state

    def startTest(self, node):
        return self.initialState == node.state

    def generateInitialNode(self):
        return Node(self.initialState)

    def showNodeState(self, node):
        print(np.matrix(node.state))

    def h(self, node):
        n = len(node.state)
        h = 0
        for i in range(n):
            for j in range(n):
                whatMustBeThere = n * i + j
                if not self.solvable:  # try to reach the alternative goal state if the puzzle is not solvable.
                    # the alternative goal state is like main goal state but last two tiles are swapped.
                    if whatMustBeThere == self.n * self.n - 2:  # e.g. 14 for n = 4
                        whatMustBeThere = self.n * self.n - 1  # e.g. 15 for n = 4
                    elif whatMustBeThere == self.n * self.n - 1:  # e.g. 15 for n = 4
                        whatMustBeThere = self.n * self.n - 2  # e.g. 14 for n = 4
                x, y = SlidingPuzzle.getIndex(node, whatMustBeThere)[0], SlidingPuzzle.getIndex(node, whatMustBeThere)[
                    1]
                h += abs(x - i) + abs(y - j)
        return h

    @staticmethod
    def getIndex(node, num):
        for i in range(len(node.state)):
            for j in range(len(node.state)):
                if node.state[i][j] == num:
                    return [i, j]
        return None

    def convertStateTo1D(self):
        list = []
        for i in range(self.n):
            for j in range(self.n):
                list.append(self.initialState[i][j])
        return list

    def getInversion(self, i):
        counter = 0
        index = -1
        list = self.convertStateTo1D()
        for j in range(len(list)):
            if list[j] == i:
                index = j
        for j in range(index + 1, self.n * self.n):
            if i > list[j] != 0:
                counter += 1
        return counter

    def isSolvable(self):
        sum = 0
        for i in range(2, self.n * self.n):
            sum += self.getInversion(i)
        if self.n % 2 == 1:
            return sum % 2 == 0
        else:
            zeroRow = -1
            for i in range(self.n):
                for j in range(self.n):
                    if self.initialState[i][j] == 0:
                        zeroRow = i
            return (zeroRow + sum) % 2 == 1

    def generateRandomInitialState(self):
        randNums = random.sample(range(self.n * self.n), self.n * self.n)
        self.initialState = [[randNums[(self.n * y + x + 1) % (self.n * self.n)] for x in range(self.n)] for y in
                             range(self.n)]

    def readInitialStateFromInput(self):
        print("enter initial state of the puzzle:")
        str = ''
        for i in range(self.n):
            str += input() + ' '
        self.initialState = [[int(str[(i * 3 + j) * 2]) for j in range(self.n)] for i in range(self.n)]
