from Node import Node
from Problem import Problem
import math


class PathFinding(Problem):
    initialState = None
    goalState = None
    map = None
    indexes = {'Oradea': 0, 'Zerind': 1, 'Arad': 2, 'Timisoara': 3, 'Lugoj': 4, 'Mehadia': 5, 'Dobreta': 6,
               'Craiova': 7, 'Sibiu': 8, 'Fagaras': 9, 'Rimnicu_Vilcea': 10, 'Pitesti': 11, 'Giurgiu': 12,
               'Bucharest': 13, 'Urziceni': 14, 'Eforie': 15, 'Hirsova': 16, 'Vaslui': 17, 'Iasi': 18, 'Neamt': 19}
    cityNames = dict((v, k) for k, v in indexes.items())
    coordinates = dict(Arad=(91, 492), Bucharest=(400, 327), Craiova=(253, 288), Dobreta=(165, 299), Eforie=(562, 293),
                       Fagaras=(305, 449), Giurgiu=(375, 270), Hirsova=(534, 350), Iasi=(473, 506), Lugoj=(165, 379),
                       Mehadia=(168, 339), Neamt=(406, 537), Oradea=(131, 571), Pitesti=(320, 368),
                       Rimnicu_Vilcea=(233, 410), Sibiu=(207, 457), Timisoara=(94, 410), Urziceni=(456, 350),
                       Vaslui=(509, 444), Zerind=(108, 531))

    def __init__(self, startingCity, destination):
        PathFinding.buildMap()
        self.initialState = startingCity
        self.goalState = destination

    def actionCost(self, action):
        return PathFinding.map[PathFinding.indexes[action[0]]][PathFinding.indexes[action[1]]]

    def getResult(self, action, node):
        return Node(action[1], node.g + self.actionCost(action), node)

    def generateInitialNode(self):
        return Node(self.initialState)

    def h(self, node):
        x1, y1 = self.coordinates[self.goalState][0], self.coordinates[self.goalState][1]
        x2, y2 = self.coordinates[node.state][0], self.coordinates[node.state][1]
        return math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2))

    def goalTest(self, node):
        return node.state == self.goalState

    def getActions(self, node):
        actions = []
        index = self.indexes[node.state]
        for i in range(len(PathFinding.map)):
            if (PathFinding.map[index][i]) != float('inf'):
                actions.append([node.state, PathFinding.cityNames[i]])
        return actions

    def showNodeState(self, node):
        print(node.state)

    @staticmethod
    def pathCost(node):
        return node.g

    @staticmethod
    def buildMap():
        PathFinding.map = [[0 for x in range(20)] for y in range(20)]
        PathFinding.map[PathFinding.indexes['Oradea']][PathFinding.indexes['Zerind']] = \
            PathFinding.map[PathFinding.indexes['Zerind']][PathFinding.indexes['Oradea']] = 71
        PathFinding.map[PathFinding.indexes['Oradea']][PathFinding.indexes['Sibiu']] = \
            PathFinding.map[PathFinding.indexes['Sibiu']][PathFinding.indexes['Oradea']] = 151
        PathFinding.map[PathFinding.indexes['Zerind']][PathFinding.indexes['Arad']] = \
            PathFinding.map[PathFinding.indexes['Arad']][PathFinding.indexes['Zerind']] = 75
        PathFinding.map[PathFinding.indexes['Arad']][PathFinding.indexes['Sibiu']] = \
            PathFinding.map[PathFinding.indexes['Sibiu']][PathFinding.indexes['Arad']] = 140
        PathFinding.map[PathFinding.indexes['Arad']][PathFinding.indexes['Timisoara']] = \
            PathFinding.map[PathFinding.indexes['Timisoara']][PathFinding.indexes['Arad']] = 140
        PathFinding.map[PathFinding.indexes['Timisoara']][PathFinding.indexes['Lugoj']] = \
            PathFinding.map[PathFinding.indexes['Lugoj']][PathFinding.indexes['Timisoara']] = 111
        PathFinding.map[PathFinding.indexes['Lugoj']][PathFinding.indexes['Mehadia']] = \
            PathFinding.map[PathFinding.indexes['Mehadia']][PathFinding.indexes['Lugoj']] = 70
        PathFinding.map[PathFinding.indexes['Mehadia']][PathFinding.indexes['Dobreta']] = \
            PathFinding.map[PathFinding.indexes['Dobreta']][PathFinding.indexes['Mehadia']] = 75
        PathFinding.map[PathFinding.indexes['Dobreta']][PathFinding.indexes['Craiova']] = \
            PathFinding.map[PathFinding.indexes['Craiova']][PathFinding.indexes['Dobreta']] = 120
        PathFinding.map[PathFinding.indexes['Craiova']][PathFinding.indexes['Pitesti']] = \
            PathFinding.map[PathFinding.indexes['Pitesti']][PathFinding.indexes['Craiova']] = 138
        PathFinding.map[PathFinding.indexes['Craiova']][PathFinding.indexes['Rimnicu_Vilcea']] = \
            PathFinding.map[PathFinding.indexes['Rimnicu_Vilcea']][PathFinding.indexes['Craiova']] = 146
        PathFinding.map[PathFinding.indexes['Pitesti']][PathFinding.indexes['Rimnicu_Vilcea']] = \
            PathFinding.map[PathFinding.indexes['Rimnicu_Vilcea']][PathFinding.indexes['Pitesti']] = 97
        PathFinding.map[PathFinding.indexes['Pitesti']][PathFinding.indexes['Bucharest']] = \
            PathFinding.map[PathFinding.indexes['Bucharest']][PathFinding.indexes['Pitesti']] = 101
        PathFinding.map[PathFinding.indexes['Rimnicu_Vilcea']][PathFinding.indexes['Sibiu']] = \
            PathFinding.map[PathFinding.indexes['Sibiu']][PathFinding.indexes['Rimnicu_Vilcea']] = 80
        PathFinding.map[PathFinding.indexes['Sibiu']][PathFinding.indexes['Fagaras']] = \
            PathFinding.map[PathFinding.indexes['Fagaras']][PathFinding.indexes['Sibiu']] = 99
        PathFinding.map[PathFinding.indexes['Fagaras']][PathFinding.indexes['Bucharest']] = \
            PathFinding.map[PathFinding.indexes['Bucharest']][PathFinding.indexes['Fagaras']] = 211
        PathFinding.map[PathFinding.indexes['Bucharest']][PathFinding.indexes['Giurgiu']] = \
            PathFinding.map[PathFinding.indexes['Giurgiu']][PathFinding.indexes['Bucharest']] = 90
        PathFinding.map[PathFinding.indexes['Bucharest']][PathFinding.indexes['Urziceni']] = \
            PathFinding.map[PathFinding.indexes['Urziceni']][PathFinding.indexes['Bucharest']] = 85
        PathFinding.map[PathFinding.indexes['Urziceni']][PathFinding.indexes['Hirsova']] = \
            PathFinding.map[PathFinding.indexes['Hirsova']][PathFinding.indexes['Urziceni']] = 98
        PathFinding.map[PathFinding.indexes['Hirsova']][PathFinding.indexes['Eforie']] = \
            PathFinding.map[PathFinding.indexes['Eforie']][PathFinding.indexes['Hirsova']] = 86
        PathFinding.map[PathFinding.indexes['Urziceni']][PathFinding.indexes['Vaslui']] = \
            PathFinding.map[PathFinding.indexes['Vaslui']][PathFinding.indexes['Urziceni']] = 142
        PathFinding.map[PathFinding.indexes['Vaslui']][PathFinding.indexes['Iasi']] = \
            PathFinding.map[PathFinding.indexes['Iasi']][PathFinding.indexes['Vaslui']] = 92
        PathFinding.map[PathFinding.indexes['Iasi']][PathFinding.indexes['Neamt']] = \
            PathFinding.map[PathFinding.indexes['Neamt']][PathFinding.indexes['Iasi']] = 87
        for i in range(len(PathFinding.map)):
            for j in range(len(PathFinding.map)):
                if PathFinding.map[i][j] == 0:
                    PathFinding.map[i][j] = float('inf')
