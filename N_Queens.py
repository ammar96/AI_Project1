import copy

from Node import Node
from Problem import Problem


class N_Queens(Problem):
    n = None
    initialState = None

    def __init__(self, n=8):
        self.n = n
        # self.initialState = random.sample(range(8), 8)
        self.initialState = [6, 5, 4, 3, 2, 1, 0, 7]

    def actionCost(self, action):
        return 1

    def goalTest(self, node):
        for i in range(self.n):
            for j in range(i + 1, self.n):
                if abs(node.state[i] - node.state[j]) == abs(i - j):
                    return False
        return True

    def getActions(self, node):
        if node.g == self.n:
            return []
        return range(self.n)

    def h(self, node):
        return 0

    def getResult(self, action, node):
        newState = copy.deepcopy(node.state)
        newState[action], newState[node.g] = newState[node.g], newState[action]
        return Node(newState, node.g + 1, node)

    def generateInitialNode(self):
        return Node(self.initialState)

    def showNodeState(self, node):
        print(node.state)

    def coutNumQueensMainDiag(self, node, a):
        if abs(a) >= self.n:
            print('coutNumQueensMainDiag')
            exit(-1)
        counter = 0
        list = []
        if a >= 0:
            for i in range(self.n - a):
                if (node.state[i] == i + a):
                    counter += 1
                    list.append(i)
        else:
            for i in range(-a, self.n):
                if (node.state[i] == i + a):
                    counter += 1
                    list.append(i)
        return [counter, list]

    # TODO: complete this method
    def coutNumQueensAntiDiag(self, node, a):
        if abs(a) >= self.n:
            print('coutNumQueensAntiDiag')
            exit(-1)
        counter = 0
        list = []
        if a >= 0:
            for i in range(self.n - a):
                if (node.state[i] + i == self.n - 1):
                    counter += 1
                    list.append(i)
        else:
            for i in range(-a, self.n):
                if (node.state[i] == i + a):
                    counter += 1
                    list.append(i)
        return [counter, list]

    @staticmethod
    def pathCost(node):
        return node.g
