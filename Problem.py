import abc


class Problem(metaclass=abc.ABCMeta):
    initialState = None

    @abc.abstractmethod
    def getActions(self, node):
        """returns a list of available actions in the given state"""

    @abc.abstractmethod
    def getResult(self, action, node):
        """returns the result state of performing the given action in the given state"""

    @abc.abstractmethod
    def goalTest(self, node):
        """returns True if the given state is one of the goal states"""

    @staticmethod
    @abc.abstractmethod
    def pathCost(node):
        """returns the cost of reaching to the given state from the initial state"""

    @abc.abstractmethod
    def actionCost(self, action):
        """returns the cost of the given action"""

    @abc.abstractmethod
    def h(self, node):
        """the heuristic used in the search algorithm"""

    @abc.abstractmethod
    def generateInitialNode(self):
        """generates the initial node"""

    @abc.abstractmethod
    def showNodeState(self, node):
        """show state of the given node in a suitable format"""
