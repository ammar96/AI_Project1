from Node import Node
from Problem import Problem


class Mnc(Problem):
    n = None
    initialState = None  # how many people of each group are on the left side of the river, [missionaries, cannibals, boatOnLeftSide]
    goalState = [0, 0, False]

    # each action is a 1D matrix of 2 elements showing number of people of each group going to using the boat

    def __init__(self, n=3):
        self.n = n
        self.initialState = [n, n, True]

    def actionCost(self, action):
        return 1

    def goalTest(self, node):
        return node.state == self.goalState

    def getActions(self, node):
        actions = []
        for i in range(3):
            for j in range(3):
                if self.checkActionValidity(node, [i, j]):
                    actions.append([i, j])
        return actions

    def h(self, node):
        return 0

    def getResult(self, action, node):
        if node.state[2]:
            i = 1
        else:
            i = -1
        return Node([node.state[0] - i * action[0], node.state[1] - i * action[1], not node.state[2]],
                    node.g + self.actionCost(action), node)

    def generateInitialNode(self):
        return Node(self.initialState)

    def showNodeState(self, node):
        print('O' * node.state[0], end='\t' * 3)
        print('O' * (self.n - node.state[0]))
        if node.state[2]:
            print('boat')
        else:
            print('\t' * 2, 'boat')
        print('X' * node.state[1], end='\t' * 3)
        print('X' * (self.n - node.state[1]))

    @staticmethod
    def pathCost(node):
        return node.g

    def checkActionValidity(self, node, action):
        if node.state[2]:
            i = 1
        else:
            i = -1
        peopleOnTheBoatValid = 1 <= sum(action) <= 2
        peopleOnLeftValid = 0 < node.state[0] - action[0] * i >= node.state[1] - action[1] * i >= 0 or node.state[0] == \
                                                                                                       action[0] * i
        peopleOnRightValid = 0 < self.n - node.state[0] + action[0] * i >= self.n - node.state[1] + action[
                                                                                                        1] * i >= 0 or self.n - \
                                                                                                                       node.state[
                                                                                                                           0] == \
                                                                                                                       action[
                                                                                                                           0] * (
                                                                                                                           -i)
        return peopleOnTheBoatValid and peopleOnLeftValid and peopleOnRightValid
