class Node:
    g = None
    state = None
    parent = None

    def __init__(self, state=None, g=0, parent=None):
        self.g = g
        self.state = state
        self.parent = parent

    def f(self, problem):
        return self.g + problem.h(self)

    def existsIn(self, nodes):  # also return the node which you are equivalent with
        for elem in nodes:
            return [True, elem]
        return [False, None]

    def getDepth(self):
        i = 0
        tmpNode = self
        while tmpNode.parent is not None:
            i += 1
            tmpNode = tmpNode.parent
        return i

    def showCompletePath(self, problem):
        tmpNode = self
        seq = []
        while tmpNode is not None:
            seq.append(tmpNode)
            tmpNode = tmpNode.parent
        seq.reverse()
        for elem in seq:
            problem.showNodeState(elem)
            print('_' * 100)
