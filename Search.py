from SlidingPuzzle import SlidingPuzzle
from PathFinding import PathFinding
from MnC import Mnc
from Node import Node

numGeneratedNodes = 0
numExpandedNodes = 0
maxNodesInF = 0
maxNodesInF_i = 0
maxNodesInF_g = 0


def enqueueAtEnd(problem, nodes, newFringes):
    for elem in newFringes:
        nodes.append(elem)


def enqueueAtFront(problem, nodes, newFringes):
    for elem in newFringes:
        nodes.insert(0, elem)


def enqueueBasedOnF(problem, nodes, newFringes):
    for elem in newFringes:
        i = 0
        while i < len(nodes) - 1 and nodes[i].f(problem) < elem.f(problem):
            i += 1
        nodes.insert(i, elem)


def enqueueBasedOnG(problem, nodes, newFringes):
    for elem in newFringes:
        i = 0
        while i < len(nodes) - 1 and nodes[i].g < elem.g:
            i += 1
        nodes.insert(i, elem)


def doGeneralSearch(problem, queuingFunc, graphSearch, maxDepth=float('inf')):
    global numGeneratedNodes, numExpandedNodes, maxNodesInF
    nodes = [problem.generateInitialNode()]
    visitedNodes = []
    newFringes = [nodes[0]]
    while True:
        maxNodesInF = max(maxNodesInF, len(nodes))
        if graphSearch:
            for elem in newFringes:
                visitedNodes.append(elem)
        newFringes = []
        if len(nodes) == 0:
            print('solution not found!')
            return None
        node = nodes.pop(0)
        if problem.goalTest(node):
            print('solution found!\nnumber of generated nodes: ', numGeneratedNodes, '\nnumber of expanded nodes: ',
                  numExpandedNodes, '\nmaximum number of nodes in F(the queue): ', maxNodesInF)
            if graphSearch:
                print('maximum number of nodes in E (for graph search): ', len(visitedNodes))
            print('cost of solution: ', node.g, ', and this is the found solution:')
            return node
        numExpandedNodes += 1
        for elem in problem.getActions(node):
            numGeneratedNodes += 1
            fringe = problem.getResult(elem, node)
            if (not graphSearch or not fringe.existsIn(visitedNodes)[0]) and fringe.getDepth() <= maxDepth:
                newFringes.append(fringe)
        queuingFunc(problem, nodes,
                    newFringes)  # expand the node and add its children to queue according to queuing function


def doBFS(problem, graphSearch):
    return doGeneralSearch(problem, enqueueAtEnd, graphSearch)


def doDFS(problem, graphSearch):
    return doGeneralSearch(problem, enqueueAtFront, graphSearch)


def doDepthLimitedDFS(problem, graphSearch, maxDepth):
    return doGeneralSearch(problem, enqueueAtFront, graphSearch, maxDepth)


def doAStarSearch(problem, graphSearch):
    return doGeneralSearch(problem, enqueueBasedOnF, graphSearch)


def doUniformCostSearch(problem, graphSearch):
    return doGeneralSearch(problem, enqueueBasedOnG, graphSearch)


def doIterativeDeepeningSearch(problem, graphSearch):
    i = 0
    while True:
        solution = doDepthLimitedDFS(problem, graphSearch, i)
        if solution is not None:
            return solution
        else:
            i += 1


def doBiDirectionalSearch(problem, queuingFunc, graphSearch):
    global numGeneratedNodes, numExpandedNodes, maxNodesInF_i, maxNodesInF_g
    nodes_i = [problem.generateInitialNode()]
    nodes_g = [Node(problem.goalState)]
    generatedNodes_i = []
    generatedNodes_g = []
    newFringes_i = [nodes_i[0]]
    newFringes_g = [nodes_g[0]]
    while True:
        maxNodesInF_i, maxNodesInF_g = max(maxNodesInF_i, len(nodes_i)), max(maxNodesInF_g, len(nodes_g))
        if graphSearch:
            for elem in newFringes_i:
                generatedNodes_i.append(elem)
            for elem in newFringes_g:
                generatedNodes_g.append(elem)
        newFringes_i = []
        newFringes_g = []
        if len(nodes_i) + len(nodes_g) == 0:
            print('solution not found!')
            return
        node_i = nodes_i.pop(0)
        if node_i.existsIn(nodes_g)[0]:
            print('solution found!')
            linkNodesAndShowSol(problem, node_i, node_i.existsIn(nodes_g)[1])
            showSearchDetailsInBiDirSearch()
            return
        numExpandedNodes += 1
        for elem in problem.getActions(node_i):
            numGeneratedNodes += 1
            fringe = problem.getResult(elem, node_i)
            if not graphSearch or not fringe.existsIn(generatedNodes_i)[0]:
                newFringes_i.append(fringe)
                queuingFunc(problem, nodes_i, newFringes_i)
        node_g = nodes_g.pop(0)
        if node_g.existsIn(nodes_i)[0]:
            print('solution found!')
            linkNodesAndShowSol(problem, node_g.existsIn(nodes_i)[1], node_g)
            showSearchDetailsInBiDirSearch()
            return
        numExpandedNodes += 1
        for elem in problem.getReversedActions(node_g):
            numGeneratedNodes += 1
            fringe = problem.getReversedActionResult(elem, node_g)
            if not graphSearch or not fringe.existsIn(generatedNodes_g)[0]:
                newFringes_g.append(fringe)
                queuingFunc(problem, nodes_g, newFringes_g)


def doBiDirectionalBFS(problem, graphSearch):
    doBiDirectionalSearch(problem, enqueueAtEnd, graphSearch)


def showSearchDetailsInBiDirSearch():
    print('search details:\nnumber of generated nodes: ', numGeneratedNodes, '\nnumber of expanded nodes: ',
          numExpandedNodes, '\nmaximum number of nodes in F_i(first queue): ', maxNodesInF_i,
          '\nmaximum number of nodes in F_g(second queue): ', maxNodesInF_g)


def linkNodesAndShowSol(problem, node_i, node_g):
    # first find the path to node_i from start node
    seq = []
    tmpNode = node_i.parent
    while tmpNode is not None:
        seq.append(tmpNode)
        tmpNode = tmpNode.parent
    seq.reverse()  # initial node --> node_i
    tmpNode = node_g
    # link found path to the path to goal node from node_g
    while tmpNode is not None:
        seq.append(tmpNode)
        tmpNode = tmpNode.parent
    # reverse the list and show complete solution, initial node --> goal node
    print('cost of solution: ', len(seq) - 1, ', and this is the found solution:')
    for elem in seq:
        problem.showNodeState(elem)
        print('_' * 100)


# uncomment following lines if you want to test the algorithms. bi-directional search shows the probably found solution at the end,
# so you shouldn't call showCompletePath(pf) after it (it doesn't return anything).

# pf = PathFinding('Arad', 'Vaslui')
# doAStarSearch(pf, True).showCompletePath(pf)
# sp = SlidingPuzzle()
# doBiDirectionalBFS(sp, True)
# sp = SlidingPuzzle()
# doAStarSearch(sp, True).showCompletePath(sp)
mnc = Mnc()
doIterativeDeepeningSearch(mnc, False).showCompletePath(mnc)
